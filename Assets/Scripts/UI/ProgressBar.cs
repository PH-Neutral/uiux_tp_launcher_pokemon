using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class ProgressBar : MonoBehaviour {
    public float progress {
        get => _slider.value;
        set {
            _slider.value = value;
            displayText.text = $"{value.ToString(_format)} / {_slider.maxValue.ToString(_format)}{_unit}";
        }
    }

    [SerializeField] Text displayText;

    string _unit = "";
    string _format = "";
    Slider _slider;

    private void Awake() {
        _slider = GetComponent<Slider>();
    }

    public void Setup(float min, float max, string unit, string format = "") {
        _slider.minValue = min;
        _slider.maxValue = max;
        _unit = unit;
        _format = "";
    }
}