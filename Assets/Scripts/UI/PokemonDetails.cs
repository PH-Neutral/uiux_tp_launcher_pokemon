using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using UnityEngine;
using UnityEngine.UI;

public class PokemonDetails : OverlayMenu {
    public int pokemonId { get; set; }

    [SerializeField] GameObject loadingPanel;
    [SerializeField] GameObject errorPanel;

    [Header("Detail")]
    [SerializeField] Text order;
    [SerializeField] Text pokeName;
    [SerializeField] Text speciesDesc;
    [SerializeField] Text genus;
    [SerializeField] Text height;
    [SerializeField] Text weight;
    [SerializeField] Text types;

    Coroutine hydration = null;
    bool errorEncountered;

    public override void RefreshLanguage() {
        base.RefreshLanguage();
        if(hydration != null) StopCoroutine(hydration);
        Hydrate(pokemonId);
    }

    public void Hydrate(int id, System.Action callback = null) {
        StartCoroutine(RequestDatas(id, callback));
    }
    IEnumerator RequestDatas(int id, System.Action callback) {
        errorEncountered = false;
        //StartLoading();
        LanguageData langData = DataManager.instance.chosenLanguage;
        PokeAPIClient client = DataManager.instance.apiClient;

        // pokemon data
        PokemonData pokemon = null;
        yield return client.Request<PokemonData>(id, (data, info) => {
            if(info != RequestInfo.Success) {
                NotifyError();
                return;
            }
            pokemon = data;
            order.text = pokemon.order.ToString();
            height.text = pokemon.height.ToString();
            weight.text = pokemon.weight.ToString();
        });
        if(errorEncountered) yield break;

        yield return client.Request<PokemonSpeciesData>(pokemon.species.url, (data, info) => {
            var nameList = data.names.WithLanguage(langData);
            if(nameList.Count == 1) {
                pokeName.text = nameList[0].text;
            }
            var genusList = data.genera.WithLanguage(langData);
            if(genusList.Count == 1) {
                genus.text = genusList[0].text;
            } else if(genusList.Count > 1) {
                genus.text = genusList[genusList.Count - 1].text;
            }
            var descList = data.flavorTextEntries.WithLanguage(langData);
            if(descList.Count == 1) {
                speciesDesc.text = descList[0].text;
            } else if(descList.Count > 1) {
                speciesDesc.text = descList[descList.Count - 1].text;
            }
        });

        foreach(var typeValue in pokemon.types) {
            yield return client.Request<TypeData>(typeValue.nameUrl.url, (data, info) => {
                var nameList = data.names.WithLanguage(langData);
                if(nameList.Count == 1) {
                    types.text += nameList[0].text + " / ";
                }
            });
        }

        callback?.Invoke();
        yield break;
    }

    void NotifyError() {
        Debug.LogWarning("Error when requesting data");
        errorEncountered = true;
        return;
        //StopLoading();
        //errorPanel.SetActive(true);
    }
    void StopLoading() => StartLoading(false);
    void StartLoading(bool loading = true) {
        errorPanel.SetActive(false);
        loadingPanel.SetActive(loading);
    }
}