using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using NeutralCode.PokeAPI;

public class Menu : MonoBehaviour {
    public virtual Menu previousMenu { get; set; }

    [SerializeField] UnityEvent onShow = null;
    [SerializeField] UnityEvent onHide = null;

    Menu _currentMenu = null;

    protected virtual void Awake() {
        previousMenu = null;
    }

    public virtual void RefreshLanguage() {
        // do nothing
    }

    public void SwitchToMenu(Menu menu) {
        HideCurrentMenu();
        _currentMenu = menu;
        ShowCurrentMenu();
    }
    public void HideCurrentMenu() {
        _currentMenu?.Hide();
    }
    public void ShowCurrentMenu() {
        _currentMenu?.Show();
    }

    public void Hide() => Show(false);
    public void Show(bool show = true) {
        gameObject.SetActive(show);
        if(show) {
            onShow?.Invoke();
        } else {
            onHide?.Invoke();
        }
    }
}