using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingMenu : Menu {
    [SerializeField] ProgressBar progressBar;
    [SerializeField] Text infoText;

    public void InitializeProgressBar(int elementCount) {
        progressBar.Setup(0f, elementCount, "", "0");
    }
    public void UpdateProgress(int progress) {
        progressBar.progress = progress;
    }
    public void SetInfoText(string text) {
        infoText.text = text;
    }
}