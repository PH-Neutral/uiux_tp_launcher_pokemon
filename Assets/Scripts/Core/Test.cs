using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public int frameCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ParentRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log($"Frame n�{frameCount++}");
    }

    IEnumerator ParentRoutine() {
        Debug.Log("start routine");

        Debug.Log("before child routine");
        yield return ChildRoutine();
        Debug.Log("after child routine");

        Debug.Log("end routine");
    }

    IEnumerator ChildRoutine() {
        Debug.Log("I am a useless routine :(");
        yield break;
    }
}
