using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NeutralCode.PokeAPI;

public class DataManager : MonoBehaviour {
    public static DataManager instance;

    public bool isInitialized { get; private set; }

    public PokeAPIClient apiClient;
    public int defaultLanguageId;
    [SerializeField] int[] pokedexIds;
    [SerializeField] LoadingMenu loadingMenu;
    [SerializeField] Menu mainMenu;
    [SerializeField] bool dontFetchData = true;

    [HideInInspector] public LanguageData chosenLanguage = null;
    [HideInInspector] public MenuController menuController = null;

    int _elemCount = 0, _elemFetched = 0;
    List<IEnumerator> _fetchQueue = new List<IEnumerator>();

    private void Awake() {
        if(instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else if(instance != this) {
            Destroy(gameObject);
        }

        StartCoroutine(Initialize());
    }

    IEnumerator Initialize() {
        mainMenu.Hide();
        loadingMenu.Show();

        //menuController = FindObjectOfType<MenuController>();
        // fetch the default language only
        RequestInfo requestInfo = RequestInfo.UnknownStatus;
        // fetch language set
        yield return FetchSet<LanguageData>((array, info) => {
            foreach(var language in array) {
                if(language.id == defaultLanguageId) chosenLanguage = language;
            }
        });
        // fetch version set
        yield return FetchSet<VersionData>();
        // fetch versionGround set
        yield return FetchSet<VersionGroupData>();
        // fetch generation set
        yield return FetchSet<GenerationData>();
        // fetch colo set
        yield return FetchSet<PokemonColorData>();
        // fetch type set
        yield return FetchSet<TypeData>();
        // fetch pokemon set
        foreach(var id in pokedexIds) {
            yield return PullPokedex(id);
        }

        loadingMenu.InitializeProgressBar(_elemCount);
        if(!dontFetchData) {
            foreach(var request in _fetchQueue) {
                yield return request;
            }
            loadingMenu.SetInfoText("Done !");

            if(chosenLanguage == null) {
                Debug.LogError($"Couldn't even fetch the language ! ({requestInfo})");
                yield break;
            }
            requestInfo = RequestInfo.Success;
        } else {
            _fetchQueue.Clear();
            loadingMenu.SetInfoText("(DEBUG) No data pulled.");
        }

        // when all the initializing is done
        isInitialized = true;
        loadingMenu.Hide();
        mainMenu.Show();
        //menuController.OnDataManagerInitialized();
        yield break;
    }
    IEnumerator FetchSet<T>(System.Action<T[], RequestInfo> callback = null) where T : APIResource, new() => FetchSet(-1, callback);
    IEnumerator FetchSet<T>(int maxCount, System.Action<T[], RequestInfo> callback = null) where T : APIResource, new() {
        yield return apiClient.RequestSet<T>((set, info) => {
            if(info != RequestInfo.Success) {
                Debug.LogWarning(typeof(T).Name + " set could not be fetched!");
                return;
            }
            _elemCount += set.results.Count;
            _fetchQueue.Add(apiClient.RequestMany(maxCount,
                () => {
                    loadingMenu.SetInfoText("Fetching " + typeof(T).Name + "s...");
                },
                (elem, info) => {
                    if(info == RequestInfo.Success) loadingMenu.UpdateProgress(++_elemFetched);
                },
                callback
                ));
        });
    }

    IEnumerator PullPokedex(int id) {
        yield return apiClient.Request<PokedexData>(id, (data, info) => {
            if(info != RequestInfo.Success) {
                Debug.LogWarning("Error when pulling pokedex");
                return;
            }
            _fetchQueue.Add(PullPokemons(data, () => loadingMenu.SetInfoText("Fetching PokemonDatas from Pokedex " + id + "...")));
            
        });
    }
    IEnumerator PullPokemons(PokedexData pokedex, System.Action startCallback = null) {
        startCallback();
        foreach(var entry in pokedex.pokemonEntries) {
            yield return PullPokemon(entry.nameUrl.url);
        }
    }
    IEnumerator PullPokemon(string url) {
        PokemonData pokemon = null;
        yield return apiClient.Request<PokemonData>(url, (data, info) => {
            pokemon = data;
        });
        if(pokemon == null) yield break;

        // species
        PokemonSpeciesData species = null;
        Debug.LogWarning($"For pokemon: {pokemon.name}, species has url = {pokemon.species.url}");
        yield return apiClient.Request<PokemonSpeciesData>(pokemon.species.url, (data, info) => {
            species = data;
        });
        if(species != null) {
            // habitat
            yield return apiClient.Request<PokemonHabitatData>(species.habitat.url);

        }
        // abilities
        foreach(var entry in pokemon.abilities) {
            yield return apiClient.Request<AbilityData>(entry.nameUrl.url);
        }
        // stats
        foreach(var entry in pokemon.stats) {
            yield return apiClient.Request<StatData>(entry.nameUrl.url);
        }
    }
}