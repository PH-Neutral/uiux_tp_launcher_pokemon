/*/
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using NeutralCode.PokeAPI.SimpleJSON;

namespace NeutralCode.PokeAPI {
    [System.Serializable]
    public class PokeDataFetcher : MonoBehaviour {
        public PokeAPIClient client;

        Dictionary<System.Type, Dictionary<string, APIResource>> _dataTypeDic = new Dictionary<System.Type, Dictionary<string, APIResource>>();
        Dictionary<string, Texture2D> _textureDic = new Dictionary<string, Texture2D>();
        List<IEnumerator> _requestList = new List<IEnumerator>();
        bool _allTypesPulled = false;
        int _nbCalls = 0;

        public PokeDataFetcher() {
            _allTypesPulled = false;
            _nbCalls = 0;
            _dataTypeDic = new Dictionary<System.Type, Dictionary<string, APIResource>>();
            _textureDic = new Dictionary<string, Texture2D>();
            _requestList = new List<IEnumerator>();
        }

        #region PUBLIC_REQUESTS

        public IEnumerator RequestTexture(string url, System.Action<Texture2D, APIResult> callback) {
            var request = GetTexture(url, false, callback);
            _requestList.Add(request);
            if(_requestList.Count > 1) {
                yield return new WaitUntil(() => _requestList.Count == 1);
            }
            yield return request;
            _requestList.Remove(request);
        }
        public IEnumerator RequestData<T>(int id, System.Action<APIResource, APIResult> callback) where T : APIResource 
            => RequestData<T>(GetUrl<T>(id), callback);
        public IEnumerator RequestData<T>(string url, System.Action<APIResource, APIResult> callback) where T : APIResource {
            var request = GetRequest<T>(url, callback);
            _requestList.Add(request);
            if(_requestList.Count > 1) {
                yield return new WaitUntil(() => _requestList.Count == 1);
            }
            yield return request;
            _requestList.Remove(request);
        }
        IEnumerator GetRequest<T>(string url, System.Action<APIResource, APIResult> callback) where T : APIResource {
            if(typeof(T) == typeof(PokedexData)) yield return GetPokedexData(url, callback);
            else if(typeof(T) == typeof(PokemonData)) yield return GetPokemonData(url, callback);
            else if(typeof(T) == typeof(PokemonSpeciesData)) yield return GetSpeciesData(url, callback);
            else if(typeof(T) == typeof(PokemonHabitatData)) yield return GetHabitatData(url, callback);
            else if(typeof(T) == typeof(TypeData)) yield return GetTypeData(url, callback);
            else if(typeof(T) == typeof(AbilityData)) yield return GetAbilityData(url, callback);
            else if(typeof(T) == typeof(RegionData)) yield return GetRegionData(url, callback);
            else if(typeof(T) == typeof(GenerationData)) yield return GetGenerationData(url, callback);
            else if(typeof(T) == typeof(EvolutionChainData)) yield return GetEvolutionChainData(url, callback);
            else callback(null, APIResult.ResourceTypeNotFound);
            yield break;
        }
        string GetUrl<T>(int id) {
            if(typeof(T) == typeof(PokedexData)) return webURL + "pokedex/" + id;
            else if(typeof(T) == typeof(PokemonData)) return webURL + "pokemon/" + id;
            else if(typeof(T) == typeof(PokemonSpeciesData)) return webURL + "pokemon-species/" + id;
            else if(typeof(T) == typeof(PokemonHabitatData)) return webURL + "pokemon-habitat/" + id;
            else if(typeof(T) == typeof(TypeData)) return webURL + "type/" + id;
            else if(typeof(T) == typeof(AbilityData)) return webURL + "ability/" + id;
            else if(typeof(T) == typeof(RegionData)) return webURL + "region/" + id;
            else if(typeof(T) == typeof(GenerationData)) return webURL + "generation/" + id;
            else if(typeof(T) == typeof(EvolutionChainData)) return webURL + "evolution-chain/" + id;
            else return null;
        }

        #endregion

        #region SPECIFIC_REQUESTS

        //IEnumerator GetPokedexData(int id, System.Action<PokedexData, APIResult> result) => GetPokedexData(webURL + "pokedex/" + id, result);
        IEnumerator GetPokedexData(string url, System.Action<PokedexData, APIResult> result) {
            PokedexData pokedexData = null;
            JSONNode pokedexNode = null;
            APIResult info = APIResult.None;
            yield return GetData<PokedexData>(url, (cachedPokedex, node, resultInfo) => {
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;

                pokedexNode = node;
                pokedexData = cachedPokedex;
                if(pokedexData == null) {
                    pokedexData = new PokedexData(url, node["id"]);
                    pokedexData.name = node["name"];
                    var names = node["names"];
                    for(int i = 0; i < names.Count; i++) {
                        if(names[i].IsLanguage(defaultLanguage)) {
                            pokedexData.name = names[i]["name"];
                        }
                    }
                    var descriptions = node["descriptions"];
                    for(int i = 0; i < descriptions.Count; i++) {
                        if(descriptions[i].IsLanguage(defaultLanguage)) {
                            pokedexData.descriptions = descriptions[i]["description"];
                        }
                    }
                    CacheData(pokedexData);
                }
            });
            if(info == APIResult.AlreadyCached || info == APIResult.IncorrectURL || info == APIResult.WebError) {
                result(pokedexData, info);
                yield break;
            }

            // pull pokemons
            var pokemonEntries = pokedexNode["pokemon_entries"];
            for(int i = 0; i < pokemonEntries.Count; i++) {
                yield return GetSpeciesData(pokemonEntries[i]["pokemon_species"]["url"], (species, resultInfo) => {
                    if(species != null) pokedexData.pokemonEntries.Add(species);
                });
            }

            result(pokedexData, info);
            yield break;
        }
        //IEnumerator GetPokemonData(int id, System.Action<PokemonData, APIResult> result) => GetPokemonData(webURL + "pokemon/" + id, result);
        IEnumerator GetPokemonData(string url, System.Action<PokemonData, APIResult> result) {
            APIResult info = APIResult.None;
            PokemonData pokemonData = null;
            JSONNode pokemonNode = null;
            yield return GetData<PokemonData>(url, (cachedPoke, node, resultInfo) => {
                //if(debugMode) Debug.Log($"Pokemon => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;

                //Debug.Log("cached poke = " + cachedPoke);
                pokemonNode = node;
                pokemonData = cachedPoke;
                if(pokemonData == null) {
                    pokemonData = new PokemonData(url, node["id"]);
                    pokemonData.order = node["order"];
                    pokemonData.name = node["name"];
                    pokemonData.height = node["height"];
                    pokemonData.weight = node["weight"];
                    pokemonData.statsDat.hp = node["stats"][0]["base_stat"];
                    pokemonData.statsDat.attack = node["stats"][1]["base_stat"];
                    pokemonData.statsDat.defence = node["stats"][2]["base_stat"];
                    pokemonData.statsDat.attackSpe = node["stats"][3]["base_stat"];
                    pokemonData.statsDat.defenceSpe = node["stats"][4]["base_stat"];
                    pokemonData.statsDat.speed = node["stats"][5]["base_stat"];

                    CacheData(pokemonData);
                }
                //Debug.Log("new poke == null ? " + (pokemonData is null));
            });
            if(info == APIResult.IncorrectURL || info == APIResult.WebError || info == APIResult.AlreadyCached) {
                result(pokemonData, info);
                yield break;
            }

            // pull all types
            yield return PullAllTypesIfNecessary();
            // link types
            var types = pokemonNode["types"];
            TypeData type;
            for(int i = 0; i < types.Count; i++) {
                var typeURL = types[i]["type"]["url"];
                type = GetCachedData<TypeData>(typeURL);
                if(type != null) pokemonData.typesDat.Add(type);
            }
            // pull species
            string speciesUrl = pokemonNode["species"]["url"];
            if(!string.IsNullOrEmpty(speciesUrl)) {
                yield return GetSpeciesData(speciesUrl, (species, resultInfo) => {
                    pokemonData.speciesDat = species;
                });
            }
            // pull abilities
            var abilities = pokemonNode["abilities"];
            for(int i = 0; i < abilities.Count; i++) {
                var abilityURL = abilities[i]["ability"]["url"];
                yield return GetAbilityData(abilityURL, (ability, resultInfo) => {
                    if(ability != null) pokemonData.abilitiesDat.Add(ability);
                });
            }
            // pull sprites
            var sprites = pokemonNode["sprites"];
            string iconUrl = sprites["front_default"];
            if(!string.IsNullOrEmpty(iconUrl)) {
                yield return GetTexture(iconUrl, false, (tex, resultInfo) => {
                    if(tex == null) return;
                    pokemonData.icon = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one * 0.5f);
                });
            }
            string spriteUrl = sprites["other"]["official-artwork"]["front_default"];
            if(!string.IsNullOrEmpty(spriteUrl)) {
                yield return GetTexture(spriteUrl, false, (tex, resultInfo) => {
                    if(tex == null) return;
                    pokemonData.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one * 0.5f);
                });
            }

            result(pokemonData, info);
            yield break;
        }
        IEnumerator PullAllTypesIfNecessary() {
            if(_allTypesPulled) yield break;

            // pull types
            int index = 0;
            TypeData foundType = null;
            do {
                yield return GetTypeData(webURL + "type/" + ++index + "/", (type, resultInfo) => {
                    foundType = type;
                    //Debug.Log($"found type = {foundType}");
                });
            } while(foundType != null);

            // link types
            TypeData type;
            foreach(var pokeData in _dataTypeDic[typeof(TypeData)].Values) {
                type = (TypeData)pokeData;
                foreach(var typeURL in type.damageFromDicURL.Keys) {
                    yield return GetTypeData(typeURL, (cachedType, resultInfo) => {
                        if(resultInfo == APIResult.IncorrectURL || resultInfo == APIResult.WebError) return;

                        if(!type.damageFromDic.Keys.Any(t => t.url.Equals(typeURL))) {
                            type.damageFromDic.Add(cachedType, type.damageFromDicURL[typeURL]);
                        }
                    });
                }
                foreach(var typeURL in type.damageToDicURL.Keys) {
                    yield return GetTypeData(typeURL, (cachedType, resultInfo) => {
                        if(resultInfo == APIResult.IncorrectURL || resultInfo == APIResult.WebError) return;

                        if(!type.damageToDic.Keys.Any(t => t.url.Equals(typeURL))) {
                            type.damageToDic.Add(cachedType, type.damageToDicURL[typeURL]);
                        }
                    });
                }
            }
            type = null;
            _allTypesPulled = true;

            foreach(var pokeData in _dataTypeDic[typeof(TypeData)].Values) {
                type = (TypeData)pokeData;
                string str = $"+++ {type.ToString("short")} +++";
                str += $"\n+ Dmg To (urls:{type.damageToDicURL.Count}, types:{type.damageToDic.Count}):";
                foreach(var typeDmgTo in type.damageToDic.Keys) {
                    str += $"\n > {typeDmgTo.ToString("short")}";
                }
                str += $"\n+ Dmg From (urls:{type.damageFromDicURL.Count}, types:{type.damageFromDic.Count}):";
                foreach(var typeDmgFrom in type.damageFromDic.Keys) {
                    str += $"\n > {typeDmgFrom.ToString("short")}";
                }
                //if(debugMode) Debug.Log(str);
            }
            type = null;
        }
        IEnumerator GetTypeData(string url, System.Action<TypeData, APIResult> result) {
            APIResult info = APIResult.None;
            TypeData typeData = null;
            yield return GetData<TypeData>(url, (cachedType, node, resultInfo) => {
                //if(debugMode) Debug.Log($"Type => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) {
                    return;
                }

                typeData = cachedType;
                if(cachedType == null) {
                    typeData = new TypeData(url, node["id"]);
                    typeData.name = node["name"];
                    // get dmg relations to other types
                    var dmgRelations = node["damage_relations"];
                    foreach(var key in dmgRelations.Keys) {
                        var dmgRelation = dmgRelations[key];
                        //Debug.Log(key);
                        // choose which dictionary to use
                        bool isTo = key.EndsWith("to");
                        bool isFrom = key.EndsWith("from");
                        if(!isTo && !isFrom) continue;
                        // choose the multiplier to apply
                        float multiplier = key.StartsWith("no") ? 0f :
                        (key.StartsWith("half") ? 0.5f :
                        (key.StartsWith("double") ? 2f : 1f));
                        for(int i = 0; i < dmgRelation.Count; i++) {
                            // add the type url to the selected dictionary
                            var subURL = dmgRelation[i]["url"];
                            //dic[subURL] = multiplier;
                            if(isTo) {
                                typeData.damageToDicURL[subURL] = multiplier;
                            } else if(isFrom) {
                                typeData.damageFromDicURL[subURL] = multiplier;
                            }
                        }
                    }
                } else {
                }
                CacheData(typeData);
            });
            if(info == APIResult.IncorrectURL || info == APIResult.WebError || info == APIResult.AlreadyCached) {
                result(typeData, info);
                yield break;
            }
            
            result(typeData, info);
            yield break;
        }
        IEnumerator GetAbilityData(string url, System.Action<AbilityData, APIResult> result) {
            AbilityData abilityData = null;
            APIResult info = APIResult.None;
            yield return GetData<AbilityData>(url, (ability, node, resultInfo) => {
                //if(debugMode) Debug.Log($"Ability => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;

                abilityData = ability;
                if(abilityData == null) {
                    abilityData = new AbilityData(url, node["id"]);
                    abilityData.name = node["name"];
                    // get the description for default language
                    var entries = node["effect_entries"];
                    for(int i = 0; i < entries.Count; i++) {
                        var entry = entries[i];
                        if(entry.IsLanguage(defaultLanguage)) {
                            abilityData.shortDescription = entry["short_effect"];
                        }
                    }
                    CacheData(abilityData);
                }
            });
            result(abilityData, info);
            yield break;
        }
        IEnumerator GetSpeciesData(string url, System.Action<PokemonSpeciesData, APIResult> result) {
            PokemonSpeciesData speciesData = null;
            APIResult info = APIResult.None;
            JSONNode speciesNode = null;
            yield return GetData<PokemonSpeciesData>(url, (species, node, resultInfo) => {
                //if(debugMode) Debug.Log($"PokemonSpecies => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;

                speciesNode = node;
                speciesData = species;
                if(speciesData == null) {
                    speciesData = new PokemonSpeciesData(url, node["id"]);
                    speciesData.name = node["name"];
                    speciesData.order = node["order"];
                    speciesData.isBaby = node["is_baby"];
                    speciesData.isLegendary = node["is_legendary"];
                    speciesData.isMythical = node["is_mythical"];
                    string colorName = node["color"]["name"];
                    speciesData.color = colorName.TryGetColor();
                    var descriptions = node["genera"];
                    for(int i = 0; i < descriptions.Count; i++) {
                        var entry = descriptions[i];
                        if(entry.IsLanguage(defaultLanguage)) {
                            speciesData.shortDescription = entry["genus"];
                        }
                    }
                    CacheData(speciesData);
                }
            });
            if(info == APIResult.AlreadyCached || info == APIResult.IncorrectURL || info == APIResult.WebError) {
                result(speciesData, info);
                yield break;
            }

            // pull evolutionChain
            string evolutionChainUrl = speciesNode["evolution_chain"]["url"];
            if(!string.IsNullOrEmpty(evolutionChainUrl)) {
                yield return GetEvolutionChainData(evolutionChainUrl, (chain, resultInfo) => {
                    speciesData.evolutionChain = chain;
                });
            }

            // pull habitat
            string habitatUrl = speciesNode["habitat"]["url"];
            if(!string.IsNullOrEmpty(habitatUrl)) {
                yield return GetHabitatData(habitatUrl, (habitat, resultInfo) => {
                    speciesData.habitat = habitat;
                });
            }

            result(speciesData, info);
            yield break;
        }
        IEnumerator GetEvolutionChainData(string url, System.Action<EvolutionChainData, APIResult> result) {
            EvolutionChainData chainData = null;
            APIResult info = APIResult.None;
            JSONNode chainNode = null;
            yield return GetData<EvolutionChainData>(url, (cachedResult, node, resultInfo) => {
                //if(debugMode) Debug.Log($"EvolutionChain => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;
                chainNode = node;
                chainData = cachedResult;
                if(chainData == null) {
                    chainData = new EvolutionChainData(url, node["id"]);
                    CacheData(chainData);
                }
            });
            if(info == APIResult.IncorrectURL || info == APIResult.WebError || info == APIResult.AlreadyCached) {
                result(chainData, info);
                yield break;
            }

            // get evolutions
            var chain = chainNode["chain"];
            yield return GetEvolutionRecursive(chain, chainData.speciesChain, null);

            result(chainData, info);
            yield break;
        }
        IEnumerator GetEvolutionRecursive(JSONNode node, EvolutionChainData.Evolution currentEvolution, EvolutionChainData.Evolution parentEvolution) {
            PokemonSpeciesData currentSpecies = null;
            yield return GetSpeciesData(node["species"]["url"], (species, resultInfo) => {
                if(species != null) {
                    currentSpecies = species;
                }
            });

            if(parentEvolution != null) {
                parentEvolution.nextEvolutions.Add(currentEvolution);
                parentEvolution.species.evolutions.Add(currentSpecies);
            }
            currentEvolution.previousEvolution = parentEvolution;
            currentEvolution.species = currentSpecies;

            for(int i = 0; i < node["evolves_to"].Count; i++) {
                yield return GetEvolutionRecursive(node["evolves_to"][i], new EvolutionChainData.Evolution(), currentEvolution);
            }
            yield break;
        }
        IEnumerator GetHabitatData(string url, System.Action<PokemonHabitatData, APIResult> result) {
            PokemonHabitatData habitatData = null;
            APIResult info = APIResult.None;
            yield return GetData<PokemonHabitatData>(url, (cachedHabitat, node, resultInfo) => {
                //if(debugMode) Debug.Log($"Habitat => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;

                habitatData = cachedHabitat;
                if(habitatData == null) {
                    habitatData = new PokemonHabitatData(url, node["id"]);
                    habitatData.name = node["name"];
                    var entries = node["names"];
                    for(int i = 0; i < entries.Count; i++) {
                        if(entries[i].IsLanguage(defaultLanguage)) {
                            habitatData.name = entries[i]["name"];
                        }
                    }
                    CacheData(habitatData);
                }
            });
            if(info == APIResult.AlreadyCached || info == APIResult.IncorrectURL || info == APIResult.WebError) {
                result(habitatData, info);
                yield break;
            }

            result(habitatData, info);
            yield break;
        }
        IEnumerator GetRegionData(string url, System.Action<RegionData, APIResult> result) {
            RegionData regionData = null;
            JSONNode regionNode = null;
            APIResult info = APIResult.None;
            yield return GetData<RegionData>(url, (cachedRegion, node, resultInfo) => {
                //if(debugMode) Debug.Log($"Region => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;

                regionNode = node;
                regionData = cachedRegion;
                if(regionData == null) {
                    regionData = new RegionData(url, node["id"]);
                    regionData.name = node["name"];
                    var entries = node["names"];
                    for(int i = 0; i < entries.Count; i++) {
                        if(entries[i].IsLanguage(defaultLanguage)) {
                            regionData.name = entries[i]["name"];
                        }
                    }
                    CacheData(regionData);
                }
            });
            if(info == APIResult.AlreadyCached || info == APIResult.IncorrectURL || info == APIResult.WebError) {
                result(regionData, info);
                yield break;
            }

            // pull generation
            string mainGenerationUrl = regionNode["main_generation"]["url"];
            if(!string.IsNullOrEmpty(mainGenerationUrl)) {
                yield return GetGenerationData(mainGenerationUrl, (generation, resultInfo) => {
                    regionData.mainGenerationDat = generation;
                });
            }
            

            result(regionData, info);
            yield break;
        }
        IEnumerator GetGenerationData(string url, System.Action<GenerationData, APIResult> result) {
            GenerationData generationData = null;
            JSONNode generationNode = null;
            APIResult info = APIResult.None;
            yield return GetData<GenerationData>(url, (cachedRegion, node, resultInfo) => {
                //if(debugMode) Debug.Log($"Generation => {resultInfo}");
                info = resultInfo;
                if(info == APIResult.IncorrectURL || info == APIResult.WebError) return;

                generationNode = node;
                generationData = cachedRegion;
                if(generationData == null) {
                    generationData = new GenerationData(url, node["id"]);
                    generationData.name = node["name"];
                    var entries = node["names"];
                    for(int i = 0; i < entries.Count; i++) {
                        if(entries[i].IsLanguage(defaultLanguage)) {
                            generationData.name = entries[i]["name"];
                        }
                    }
                    CacheData(generationData);
                }
            });
            if(info == APIResult.AlreadyCached || info == APIResult.IncorrectURL || info == APIResult.WebError) {
                result(generationData, info);
                yield break;
            }

            // pull region
            string mainRegionUrl = generationNode["main_region"]["url"];
            if(!string.IsNullOrEmpty(mainRegionUrl)) {
                yield return GetRegionData(mainRegionUrl, (region, resultInfo) => {
                    generationData.mainRegionDat = region;
                });
            }

            result(generationData, info);
            yield break;
        }

        #endregion

        #region GENERIC_METHODS

        IEnumerator GetData<T>(string url, System.Action<T, JSONNode, APIResult> result) where T : APIResource {
            if(url == null || !url.StartsWith(webURL)) {
                PassResult(result, url, null, null, APIResult.IncorrectURL);
                yield break;
            }
            T data = GetCachedData<T>(url);
            if(data != null) {
                PassResult(result, url, data, null, APIResult.AlreadyCached);
                yield break;
            }
            UnityWebRequest request = UnityWebRequest.Get(url);
            yield return request.SendWebRequest();
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) Debug.Log($"|| > REQUESTED a {typeof(T).Name} at url = \"{url}\"");
            _nbCalls++;
            if(request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError) {
                PassResult(result, url, null, null, APIResult.WebError);
                yield break;
            }
            var node = JSON.Parse(request.downloadHandler.text);
            PassResult(result, url, null, node, APIResult.WebSuccess);
            yield break;
        }
        void CacheData<T>(T data, bool overwrite = false) where T : APIResource {
            //Debug.Log("data is null ? " + (data is null));
            if(data == null) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to cache null data => type: {typeof(T).Name}");
                return;
            }
            //Debug.Log("> Data will be cached...");
            if(!_dataTypeDic.ContainsKey(typeof(T))) AddDataList<T>();
            if(_dataTypeDic[typeof(T)].ContainsKey(data.url)) {
                //Debug.Log("--> data = " + data + ", is already in dico.");
                if(overwrite) {
                    _dataTypeDic[typeof(T)][data.url] = data;
                }
                return;
            }
            _dataTypeDic[typeof(T)].Add(data.url, data);
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) Debug.Log($"|| > CACHED a {typeof(T).Name} !");
            //Debug.Log($"dico {typeof(T)} count: {dataTypeDic[typeof(T)].Count}");
        }
        T GetCachedData<T>(string url) where T : APIResource {
            if(url == null) return null;
            if(!_dataTypeDic.ContainsKey(typeof(T))) {
                AddDataList<T>();
                return null;
            }
            var dataDic = _dataTypeDic[typeof(T)];
            if(!dataDic.ContainsKey(url)) return null;
            return dataDic[url] as T;
        }
        void AddDataList<T>() where T : APIResource {
            if(_dataTypeDic.ContainsKey(typeof(T))) return;
            _dataTypeDic.Add(typeof(T), new Dictionary<string, APIResource>());
        }
        public IEnumerator GetTexture(string url, bool overwrite, System.Action<Texture2D, APIResult> result) {
            Texture2D tex;
            if((tex = GetCachedTexture(url)) != null) {
                result(tex, APIResult.AlreadyCached);
                yield break;
            }
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
            yield return request.SendWebRequest();
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) Debug.Log($"|| > REQUESTED a Texture2D at url = \"{url}\"");
            _nbCalls++;
            if(request.result != UnityWebRequest.Result.Success) {
                //throw new WebRequestError(request.error);
                result(null, APIResult.WebError);
                yield break;
            }
            CacheTexture(tex, url, overwrite);
            result(((DownloadHandlerTexture)request.downloadHandler).texture, APIResult.WebSuccess);
            yield break;
        }
        void CacheTexture(Texture2D tex, string url, bool overwrite = false) {
            if(tex == null) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to cache null data => type: Texture2D");
                return;
            }
            if(_textureDic.ContainsKey(url)) {
                if(overwrite) {
                    _textureDic[url] = tex;
                }
                return;
            }
            _textureDic.Add(url, tex);
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) Debug.Log($"|| > CACHED a Texture2D !");
        }
        public Texture2D GetCachedTexture(string url) {
            if(!_textureDic.ContainsKey(url)) return null;
            return _textureDic[url];
        }
        public int GetFullDataCount() {
            int result = _textureDic.Count;
            foreach(var dic in _dataTypeDic.Values) {
                result += dic.Count;
            }
            return result;
        }
        public T[] GetArray<T>() where T : APIResource {
            if(!_dataTypeDic.ContainsKey(typeof(T))) return null;
            //Debug.Log($"dico {typeof(T).Name} Count = {dataTypeDic[typeof(T)].Count}");
            return new List<APIResource>(_dataTypeDic[typeof(T)].Values).ConvertAll(x => (T)x).ToArray();
        }
        void PassResult<T>(System.Action<T, JSONNode, APIResult> action, string url, T resource, JSONNode node, APIResult info) where T : APIResource {
            if(info == APIResult.WebError || info == APIResult.IncorrectURL) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"{typeof(T).Name} => {info} at url = {url}");
            } else {
                if(debugMode == DebugMode.Detailed) Debug.Log($"{typeof(T).Name} => {info}");
            }
            action(resource, node, info);
        }

        #endregion
    }
}
//*/