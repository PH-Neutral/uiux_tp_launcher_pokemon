using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public static class PokeAPIUtility {
        /// <summary>
        /// Returns the color associated with this name. If not recognized, it will return a clear color.
        /// </summary>
        /// <param name="colorName"></param>
        /// <returns></returns>
        public static Color TryGetColor(this string colorName) {
            switch(colorName) {
                case "white": return Color.white;
                case "black": return Color.black;
                case "grey":
                case "gray": return Color.gray;
                case "green": return Color.green;
                case "red": return Color.red;
                case "blue": return Color.blue;
                case "yellow": return Color.yellow;
                case "pink": return new Color(1f, 0.753f, 0.796f);
                case "purple": return new Color(0.5f, 0f, 0.5f);
                case "brown": return new Color(0.396f, 0.263f, 0.129f);
                default:
                    Debug.LogWarning($"The color couldn't be found. (name = {colorName})");
                    return Color.clear;
            }
        }

        public static List<TextEntry> WithLanguage(this List<TextEntry> list, LanguageData langData) => WithLanguage(list, langData.url);
        static List<TextEntry> WithLanguage(this List<TextEntry> list, string url) => list.WithCondition(entry => entry.lang.url == url);
        public static List<TextEntry> WithVersion(this List<TextEntry> list, VersionData versionData) => WithVersion(list, versionData.url);
        static List<TextEntry> WithVersion(this List<TextEntry> list, string url) => list.WithCondition(entry => entry.version.url == url);
        public static List<TextEntry> WithVersionGroup(this List<TextEntry> list, VersionGroupData versionGroup) => WithVersionGroup(list, versionGroup.url);
        static List<TextEntry> WithVersionGroup(this List<TextEntry> list, string url) => list.WithCondition(entry => entry.versionGroup.url == url);

        public delegate bool TextEntryCondition(TextEntry entry);
        static List<TextEntry> WithCondition(this List<TextEntry> list, TextEntryCondition predicate) {
            if(!list.Any(entry => predicate(entry))) return new List<TextEntry>();
            else return new List<TextEntry>(list.Select((entry, i) => predicate(entry) ? entry : TextEntry.empty).Where(entry => !entry.isEmpty));
        }
    }
}