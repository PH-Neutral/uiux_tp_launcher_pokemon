using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class LanguageData : APIResource {
        public bool official;
        public string iso639;
        public string iso3166;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            official = node["official"];
            iso639 = node["iso639"];
            iso3166 = node["iso3166"];
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "language/" + id;
        }
    }
}