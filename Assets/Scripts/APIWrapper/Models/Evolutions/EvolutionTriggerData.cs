using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class EvolutionTriggerData : APIResource {
        public List<TextEntry> names;
        public List<NameUrl> pokemonSpecies;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            names = GetNames(node["names"]);
            pokemonSpecies = GetNameUrlList(node["pokemon_species"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "evolution-trigger/" + id;
        }
    }
}