using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class EvolutionChainData : APIResource {
        public string babyTriggerItem;
        public Chain chain;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            babyTriggerItem = node["baby_trigger_item"];
            chain = new Chain(this, node["chain"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "evolution-chain/" + id;
        }

        /*/
        public List<PokemonSpeciesData> GetEvolutions(PokemonSpeciesData species) {
            List<PokemonSpeciesData> list = new List<PokemonSpeciesData>();
            GetEvolutionsRecursive(species, list, speciesChain);
            return list;
        }
        bool GetEvolutionsRecursive(PokemonSpeciesData species, List<PokemonSpeciesData> list, Evolution currentEvolution) {
            foreach(var evolution in currentEvolution.nextEvolutions) {
                if(currentEvolution.species == species) {
                    list.Add(evolution.species);
                    continue;
                }
                if(GetEvolutionsRecursive(species, list, currentEvolution)) {
                    return true;
                }
            }
            return list.Count > 0;
        }

        public class Evolution {
            public Evolution previousEvolution;
            public PokemonSpeciesData species;
            public List<Evolution> nextEvolutions;

            public Evolution() : this(null, null) { }
            public Evolution(Evolution previousEvolution, PokemonSpeciesData species) : this(previousEvolution, species, new List<Evolution>()) { }
            public Evolution(Evolution previousEvolution, PokemonSpeciesData species, List<Evolution> evolutions) {
                this.previousEvolution = previousEvolution;
                this.species = species;
                this.nextEvolutions = evolutions;
            }
        }//*/
        public class Chain {
            public EvolutionChainData evolutionChain;
            public bool isBaby;
            public NameUrl species;
            public List<Chain> evolvesTo;
            public Chain evolvesFrom;

            public Chain(EvolutionChainData evolutionChain, JSONNode node, Chain parentChain = null) {
                JSONNode subNode;
                this.evolutionChain = evolutionChain;
                isBaby = node["is_baby"];
                species = GetNameUrl(node["species"]);
                // evolves to
                subNode = node["evolves_to"];
                evolvesTo = new List<Chain>();
                for(int i = 0; i < subNode.Count; i++) {
                    evolvesTo.Add(new Chain(evolutionChain, subNode[i]));
                }
                evolvesFrom = parentChain;
            }
        }
    }
}