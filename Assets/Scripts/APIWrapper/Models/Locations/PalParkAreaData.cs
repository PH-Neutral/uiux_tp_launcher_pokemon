using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class PalParkAreaData : APIResource {
        public List<TextEntry> names;
        public List<ValueData> pokemonEncounters;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            names = GetNames(node["names"]);
            pokemonEncounters = GetValueDatas(node["pokemon_encounters"],
                "pokemon_species", "base_score", "rate");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pal-park-area/" + id;
        }
    }
}