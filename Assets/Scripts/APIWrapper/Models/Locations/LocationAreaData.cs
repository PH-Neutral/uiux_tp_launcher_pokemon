using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class LocationAreaData : APIResource {
        public int gameIndex;
        public List<ValueData> encounterMethodRates;
        public NameUrl location;
        public List<TextEntry> names;
        public List<ValueData> pokemonEncounters;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            gameIndex = node["game_index"];
            encounterMethodRates = GetValueDatas(node["encounter_method_rates"],
                "encounter_method", "version_details");
            location = GetNameUrl(node["location"]);
            names = GetNames(node["names"]);
            pokemonEncounters = GetValueDatas(node["pokemon_encounters"],
                "pokemon", "version_details");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "location-area/" + id;
        }
    }
}