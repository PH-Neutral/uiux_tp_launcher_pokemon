using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class RegionData : APIResource {
        public List<NameUrl> locations;
        public NameUrl mainGeneration;
        public List<TextEntry> names;
        public List<NameUrl> pokedexes;
        public List<NameUrl> versionGroups;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            locations = GetNameUrlList(node["locations"]);
            mainGeneration = GetNameUrl(node["main_generation"]);
            names = GetNames(node["names"]);
            pokedexes = GetNameUrlList(node["pokedexes"]);
            versionGroups = GetNameUrlList(node["version_groups"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "region/" + id;
        }
    }
}