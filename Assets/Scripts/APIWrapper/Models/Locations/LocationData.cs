using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class LocationData : APIResource {
        public NameUrl region;
        public List<TextEntry> names;
        public List<NameUrl> areas;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            region = GetNameUrl(node["region"]);
            names = GetNames(node["names"]);
            areas = GetNameUrlList(node["areas"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "location/" + id;
        }
    }
}