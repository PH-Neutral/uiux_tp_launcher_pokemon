using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class ItemFlingEffectData : APIResource {
        public List<TextEntry> effectEntries;
        public List<NameUrl> items;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            effectEntries = GetTextEntries(node["effect_entries"], "effect");
            items = GetNameUrlList(node["items"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "item-fling-effect/" + id;
        }
    }
}