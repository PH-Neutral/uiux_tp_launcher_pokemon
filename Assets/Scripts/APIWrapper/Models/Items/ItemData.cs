using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class ItemData : APIResource {
        public int cost;
        public int flingPower;
        public NameUrl flingEffect;
        public List<NameUrl> attributes;
        public NameUrl category;
        public List<TextEntry> effectEntries;
        public List<TextEntry> flavorTextEntries;
        public List<ValueData> gameIndices;
        public List<TextEntry> names;
        public List<NameUrl> sprites;
        public List<ValueData> heldByPokemon;
        public string babyTriggerFor;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            cost = node["cost"];
            flingPower = node["flingPower"];
            flingEffect = GetNameUrl(node["fling_effect"]);
            attributes = GetNameUrlList(node["attributes"]);
            category = GetNameUrl(node["category"]);
            effectEntries = GetTextEntries(node["effect_entries"], "effect", "short_effect");
            flavorTextEntries = GetTextEntries(node["floavor_text_entries"], "text");
            gameIndices = GetValueDatas(node["game_indices"], "generation", "game_index");
            names = GetNames(node["names"]);
            sprites = GetSprites(node["sprites"]);
            heldByPokemon = GetValueDatas(node["held_by_pokemon"], "pokemon", "version_details");
            babyTriggerFor = node["baby_trigger_for"]["url"];
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "item/" + id;
        }
    }
}