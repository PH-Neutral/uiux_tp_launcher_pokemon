using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class ItemCategoryData : APIResource {
        public List<NameUrl> items;
        public List<TextEntry> names;
        public NameUrl pocket;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            items = GetNameUrlList(node["items"]);
            names = GetNames(node["names"]);
            pocket = GetNameUrl(node["pocket"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "item-category/" + id;
        }
    }
}