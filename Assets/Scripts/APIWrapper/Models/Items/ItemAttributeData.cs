using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class ItemAttributeData : APIResource {
        public List<TextEntry> descriptions;
        public List<NameUrl> items;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            descriptions = GetTextEntries(node["descriptions"], "description");
            items = GetNameUrlList(node["items"]);
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "item-attribute/" + id;
        }
    }
}