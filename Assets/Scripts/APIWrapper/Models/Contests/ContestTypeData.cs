using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;
using UnityEngine.UIElements;

namespace NeutralCode.PokeAPI {
    public class ContestTypeData : APIResource {
        public NameUrl berryFlavor;
        public List<ValueData> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            berryFlavor = GetNameUrl(node["berry_flavor"]);
            names = GetValueDatas(node["names"], "language", "name", "color");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "contest-type/" + id;
        }
    }
}