using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class ContestEffectData : APIResource {
        public int appeal;
        public int jam;
        public List<TextEntry> effectEntries;
        public List<TextEntry> flavorTextEntries;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            appeal = node["appeal"];
            jam = node["jam"];
            effectEntries = GetNames(node["effect_entries"]);
            flavorTextEntries = GetNames(node["flavor_text_entries"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "contest-effect/" + id;
        }
    }
}