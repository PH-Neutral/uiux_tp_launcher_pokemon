using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class SuperContestEffectData : APIResource {
        public int appeal;
        public List<TextEntry> flavorTextEntries;
        public List<NameUrl> moves;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            appeal = node["appeal"];
            flavorTextEntries = GetNames(node["flavor_text_entries"]);
            moves = GetNameUrlList(node["moves"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "super-contest-effect/" + id;
        }
    }
}