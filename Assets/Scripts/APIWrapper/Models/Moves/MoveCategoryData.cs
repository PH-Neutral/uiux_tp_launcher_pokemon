using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class MoveCategoryData : APIResource {
        public List<TextEntry> descriptions;
        public List<NameUrl> moves;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            descriptions = GetTextEntries(node["descriptions"], "description");
            moves = GetNameUrlList(node["moves"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "move-category/" + id;
        }
    }
}