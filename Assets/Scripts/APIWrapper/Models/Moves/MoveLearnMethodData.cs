using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class MoveLearnMethodData : APIResource {
        public List<TextEntry> names;
        public List<TextEntry> descriptions;
        public List<NameUrl> versionGroups;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            names = GetNames(node["names"]);
            descriptions = GetTextEntries(node["descriptions"], "description");
            versionGroups = GetNameUrlList(node["version_groups"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "move-learn-method/" + id;
        }
    }
}