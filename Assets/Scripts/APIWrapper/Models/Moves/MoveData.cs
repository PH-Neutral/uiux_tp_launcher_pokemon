using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class MoveData : APIResource {
        public int accuracy;
        public int effectChance;
        public int pp;
        public int priority;
        public int power;
        public List<NameUrl> contestComboNormalUseBefore;
        public List<NameUrl> contestComboNormalUseAfter;
        public List<NameUrl> contestComboSuperUseBefore;
        public List<NameUrl> contestComboSuperUseAfter;
        public NameUrl contestType;
        public string contestEffect;
        public NameUrl damageClass;
        public List<TextEntry> effectEntries;
        public NameUrl generation;
        public Meta meta;
        public List<TextEntry> names;
        public string superContestEffect;
        public NameUrl target;
        public NameUrl type;
        public List<NameUrl> learnedByPokemon;
        public List<TextEntry> flavorTextEntries;


        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            accuracy = node["accuracy"];
            effectChance = node["effect_chance"];
            accuracy = node["pp"];
            priority = node["priority"];
            power = node["power"];
            contestComboNormalUseBefore = GetNameUrlList(node["contest_combos"]["normal"]["use_before"]);
            contestComboNormalUseAfter = GetNameUrlList(node["contest_combos"]["normal"]["use_after"]);
            contestComboSuperUseBefore = GetNameUrlList(node["contest_combos"]["super"]["use_before"]);
            contestComboSuperUseAfter = GetNameUrlList(node["contest_combos"]["super"]["use_after"]);
            contestType = GetNameUrl(node["contest_type"]);
            contestEffect = node["contest_effect"];
            damageClass = GetNameUrl(node["damage_class"]);
            effectEntries = GetTextEntries(node["effect_entries"], "effect", "short_effect");
            generation = GetNameUrl(node["generation"]);
            meta = new Meta(node["meta"]);
            names = GetNames(node["names"]);
            superContestEffect = node["super_contest_effect"];
            target = GetNameUrl(node["target"]);
            type = GetNameUrl(node["type"]);
            learnedByPokemon = GetNameUrlList(node["learned_by_pokemon"]);
            flavorTextEntries = GetTextEntries(node["flavor_text_entries"], "flavor_text");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "move/" + id;
        }

        public class Meta {
            public NameUrl ailment;
            public NameUrl category;
            public int minHits;
            public int maxHits;
            public int minTurns;
            public int maxTurns;
            public int drain;
            public int healing;
            public int critRate;
            public int ailmentChance;
            public int flinchChance;
            public int statChance;

            public Meta(JSONNode node) {
                ailment = GetNameUrl(node["ailment"]);
                category = GetNameUrl(node["category"]);
                minHits = node["min_hits"];
                maxHits = node["max_hits"];
                minTurns = node["min_turns"];
                maxTurns = node["max_turns"];
                drain = node["drain"];
                healing = node["healing"];
                critRate = node["crit_rate"];
                ailmentChance = node["ailment_chance"];
                flinchChance = node["flinch_chance"];
                statChance = node["stat_chance"];
            }
        }
    }
}