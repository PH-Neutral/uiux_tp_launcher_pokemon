using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class BerryFirmnessData : APIResource {
        public List<NameUrl> berries;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            berries = GetNameUrlList(node["berries"]);
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "berry-firmness/" + id;
        }
    }
}
