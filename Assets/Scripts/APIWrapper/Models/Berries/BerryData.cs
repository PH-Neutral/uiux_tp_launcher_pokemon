using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class BerryData : APIResource {
        public int growthTime;
        public int maxHarvest;
        public int naturalGiftPower;
        public int size;
        public int smoothness;
        public int soilDryness;
        public NameUrl firmness;
        public List<ValueData> flavors;
        public NameUrl item;
        public NameUrl naturalGiftType;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            growthTime = node["growth_time"];
            maxHarvest = node["max_harvest"];
            naturalGiftPower = node["natural_gift_power"];
            size = node["size"];
            smoothness = node["smoothness"];
            soilDryness = node["soil_dryness"];
            firmness = GetNameUrl(node["firmness"]);
            flavors = GetValueDatas(node["flavors"], "flavor", "potency");
            item = GetNameUrl(node["item"]);
            naturalGiftType = GetNameUrl(node["natural_gift_type"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "berry/" + id;
        }
    }
}