using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class BerryFlavorData : APIResource {
        public List<ValueData> berries;
        public NameUrl contextType;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            berries = GetValueDatas(node["berries"], "berry", "potency");
            contextType = GetNameUrl(node["contest_type"]);
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "berry-flavor/" + id;
        }
    }
}
