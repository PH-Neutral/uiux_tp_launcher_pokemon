using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NeutralCode.PokeAPI.SimpleJSON;

namespace NeutralCode.PokeAPI {
    public class APIResource {
        public string url;
        public int id;
        public string name;

        public virtual void Initialize(string url, JSONNode node) {
            this.url = url;
            id = node["id"];
            name = node["name"];
        }

        public virtual string GetUrl(int id) {
            return PokeAPIClient.defaultWebUrl;
        }

        public static List<NameUrl> GetSprites(JSONNode spritesNode) {
            JSONNode tempNode, entry;
            List<NameUrl> sprites = new List<NameUrl>();
            int n = 0;
            foreach(var key in spritesNode.Keys) {
                if(n++ < 8) {
                    sprites.Add(new NameUrl(key, spritesNode[key]));
                } else if(key.Equals("other")) {
                    tempNode = spritesNode[key];
                    foreach(var tempKey in tempNode.Keys) {
                        entry = tempNode[tempKey];
                        foreach(var subKey in entry.Keys) {
                            sprites.Add(new NameUrl(tempKey + "/" + subKey, entry[subKey]));
                        }
                    }
                }
            }
            return sprites;
        }
        public static List<ValueData> GetValueDatas(JSONNode node, string nameUrlKey, params string[] keys) {
            List<ValueData> result = new List<ValueData>();
            JSONNode entry;
            for(int i = 0; i < node.Count; i++) {
                entry = node[i];
                ValueData data = new ValueData(GetNameUrl(entry[nameUrlKey]));
                foreach(var key in keys) {
                    data[key] = entry[key];
                }
                result.Add(data);
            }

            return result;
        }
        public static List<ValueData> GetValueDatas(JSONNode node, string nameUrlKey, System.Action<JSONNode, ValueData> forEach) {
            List<ValueData> result = new List<ValueData>();
            JSONNode entry;
            for(int i = 0; i < node.Count; i++) {
                entry = node[i];
                ValueData data = new ValueData(GetNameUrl(entry[nameUrlKey]));
                forEach?.Invoke(entry, data);
                result.Add(data);
            }

            return result;
        }
        public static List<TextEntry> GetNames(JSONNode node) => GetTextEntries(node, "name");
        public static List<TextEntry> GetTextEntries(JSONNode node, string textKey, string shortTextKey = null) {
            JSONNode entry;
            List<TextEntry> names = new List<TextEntry>();
            for(int i = 0; i < node.Count; i++) {
                entry = node[i];
                names.Add(new TextEntry(GetNameUrl(entry["language"]), GetNameUrl(entry["version"]), GetNameUrl(entry["version_group"]),
                    entry[textKey], shortTextKey != null ? entry[shortTextKey] : null));
            }
            return names;
        }
        public static List<NameUrl> GetNameUrlList(JSONNode node) {
            List<NameUrl> nameUrls = new List<NameUrl>();
            for(int i = 0; i < node.Count; i++) {
                nameUrls.Add(GetNameUrl(node[i]));
            }
            return nameUrls;
        }
        public static NameUrl GetNameUrl(JSONNode node) {
            return new NameUrl(node["name"]?.ToString(), node["url"]?.ToString()); ;
        }

    }
    // +++ ValueData +++ //
    public class ValueData {
        public PairedNode[] BoolValues {
            get => nodeSet.ToArray();
        }
        public NameUrl nameUrl;
        List<PairedNode> nodeSet;

        public ValueData(NameUrl nameUrl) {
            this.nameUrl = nameUrl;
            nodeSet = new List<PairedNode>();
        }

        public void Add(string key, JSONNode node) {
            if(key == null) throw new System.ArgumentNullException("Key can't be null !");
            if(Has(key)) throw new System.ArgumentException($"Key already added exception. (key: \"{key}\")");
            nodeSet.Add(new PairedNode(key, node));
        }
        public PairedNode Get(string key) {
            if(key == null) throw new System.ArgumentNullException("Key can't be null !");
            if(!Has(key)) throw new System.ArgumentException($"Key doesn't exist ! (key: \"{key}\")");
            return nodeSet.First(pair => pair.Key == key);
        }
        public bool Has(string key) {
            if(key == null) throw new System.ArgumentNullException("Key can't be null!");
            return nodeSet.Any(pair => pair.Key == key);
        }
        public void Set(string key, JSONNode node) {
            if(key == null) throw new System.ArgumentNullException("Key can't be null!");
            if(!Has(key)) Add(key, node);
            else Get(key).Node = node;
        }

        public JSONNode this[string key] {
            get => Get(key).Node;
            set => Set(key, value);
        }

        // +++ Value +++ //
        public class PairedNode {
            public string Key { get; protected set; }
            public JSONNode Node { get; set; }

            public PairedNode(string key, JSONNode node) {
                this.Key = key;
                this.Node = node;
            }
        }
        // --- End Value --- //
    }
    // --- End ValueData --- //

    // +++ TextData +++ //
    public struct TextEntry {
        public static TextEntry empty = new TextEntry() { isEmpty = true };

        public bool isEmpty;
        public NameUrl lang;
        public NameUrl version;
        public NameUrl versionGroup;
        public string text;
        public string shortText;

        public TextEntry(NameUrl lang, NameUrl version, NameUrl versionGroup, string text) : this(lang, version, versionGroup, text, "") { }
        public TextEntry(NameUrl lang, NameUrl version, NameUrl versionGroup, string text, string shortText) {
            this.lang = lang;
            this.version = version;
            this.versionGroup = versionGroup;
            this.text = text ?? "";
            this.shortText = shortText ?? "";
            this.isEmpty = false;
        }
    }
    // --- End TextData --- //

    // +++ NameUrl +++ //
    public struct NameUrl {
        public string name;
        public string url;

        public NameUrl(string name, string url) {
            this.name = StripQuotes(name);
            this.url = StripQuotes(url);
            //Debug.LogWarning("new NameUrl: " + this);
        }

        static string StripQuotes(string input) {
            if(string.IsNullOrEmpty(input)) return input;
            if(input.Length >= 2 && input.StartsWith("\"") && input.EndsWith("\"")) return input.Substring(1, input.Length - 2);
            return input;
        }

        public override string ToString() {
            return $"({name}: \"{url}\")";
        }
    }
    // --- End NameUrl --- //
}