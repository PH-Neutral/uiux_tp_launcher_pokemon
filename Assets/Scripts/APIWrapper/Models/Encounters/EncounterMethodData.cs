using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class EncounterMethodData : APIResource {
        public int order;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            order = node["order"];
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "encounter-method/" + id;
        }
    }
}