using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class ResultSet<T> : APIResource where T : APIResource, new() {
        public int totalCount;
        public string previousSet;
        public string nextSet;
        public List<NameUrl> results;

        public override void Initialize(string url, JSONNode node) {
            this.url = url;
            this.id = 0;
            this.name = $"ResultSet => \"{typeof(T).Name}\"";
            //base.Initialize(url, node);
            totalCount = node["count"];
            previousSet = node["previous"];
            nextSet = node["next"];
            results = GetNameUrlList(node["results"]);
        }

        //public string GetUrl() => GetUrl(0);
        public override string GetUrl(int id = 0) {
            string url = new T().GetUrl(0);
            return url.Substring(0, url.Length - 1);
        }
    }
}