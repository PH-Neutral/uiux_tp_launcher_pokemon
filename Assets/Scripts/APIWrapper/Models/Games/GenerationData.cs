using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class GenerationData : APIResource {
        public List<NameUrl> abilities;
        public NameUrl mainRegion;
        public List<NameUrl> moves;
        public List<TextEntry> names;
        public List<NameUrl> pokemonSpecies;
        public List<NameUrl> types;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            abilities = GetNameUrlList(node["abilities"]);
            mainRegion = GetNameUrl(node["main_region"]);
            moves = GetNameUrlList(node["moves"]);
            names = GetNames(node["names"]);
            pokemonSpecies = GetNameUrlList(node["pokemon_species"]);
            types = GetNameUrlList(node["types"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "generation/" + id;
        }
    }
}