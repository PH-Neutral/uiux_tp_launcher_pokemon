using System;
using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class VersionData : APIResource {
        public List<TextEntry> names;
        public NameUrl versionGroup;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            names = GetNames(node["names"]);
            versionGroup = GetNameUrl(node["version_group"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "version/" + id;
        }
    }
}