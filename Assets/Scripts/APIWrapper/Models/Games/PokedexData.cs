using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class PokedexData : APIResource {
        public bool isMainSeries;
        public List<TextEntry> descriptions;
        public List<TextEntry> names;
        /// <summary>
        /// The pokemon SPECIES.
        /// </summary>
        public List<ValueData> pokemonEntries;
        public NameUrl region;
        public List<NameUrl> versionGroups;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            isMainSeries = node["is_main_series"];
            descriptions = GetTextEntries(node["descriptions"], "description");
            names = GetNames(node["names"]);
            pokemonEntries = GetValueDatas(node["pokemon_entries"], "pokemon_species", "entry_number");
            region = GetNameUrl(node["region"]);
            versionGroups = GetNameUrlList(node["version_groups"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokedex/" + id;
        }

        public override string ToString() {
            return $"(Pokedex => \"{name}\" -> pokemonCount: {pokemonEntries.Count})";
        }
    }
}