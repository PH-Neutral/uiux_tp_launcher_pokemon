using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;
using static NeutralCode.PokeAPI.EvolutionChainData;

namespace NeutralCode.PokeAPI {
    public class VersionGroupData : APIResource {
        public int order;
        public NameUrl generation;
        public List<NameUrl> moveLearnMethods;
        public List<NameUrl> pokedexes;
        public List<NameUrl> regions;
        public List<NameUrl> versions;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            order = node["order"];
            generation = GetNameUrl(node["generation"]);
            moveLearnMethods = GetNameUrlList(node["move_learn_methods"]);
            pokedexes = GetNameUrlList(node["pokedexes"]);
            regions = GetNameUrlList(node["regions"]);
            versions = GetNameUrlList(node["versions"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "version-group/" + id;
        }
    }
}