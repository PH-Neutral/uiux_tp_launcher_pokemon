using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class MachineData : APIResource {
        public NameUrl item;
        public NameUrl move;
        public NameUrl versionGroup;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            item = GetNameUrl(node["item"]);
            move = GetNameUrl(node["move"]);
            versionGroup = GetNameUrl(node["version_group"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "machine/" + id;
        }
    }
}