using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class PokemonLocationAreaData : APIResource {
        public List<ValueData> locationAreas;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            locationAreas = GetValueDatas(node, "location_area",
                "version_details");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokemon/" + id + "/encounters";
        }
    }
}