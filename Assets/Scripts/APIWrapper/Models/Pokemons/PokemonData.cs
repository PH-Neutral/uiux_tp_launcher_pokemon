using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class PokemonData : APIResource {
        /*/
        public Stats statsDat;
        public PokemonSpeciesData speciesDat;
        public List<TypeData> typesDat;
        public List<AbilityData> abilitiesDat;
        public Sprite sprite;
        public Sprite icon;//*/

        public int baseExperience;
        public int height; // decimeters
        public bool isDefault;
        public int order;
        public int weight; // hectograms
        public List<ValueData> abilities;
        public List<NameUrl> forms;
        public List<ValueData> heldItems;
        public string locationAreaEncounters;
        public List<ValueData> moves;
        public NameUrl species;
        public List<NameUrl> sprites;
        public List<ValueData> stats;
        public List<ValueData> types;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            // debug
            foreach(var key in node.Keys) {
                Debug.Log(key);
            }
            // debug
            baseExperience = node["base_experience"];
            height = node["height"];
            isDefault = node["is_default"];
            order = node["order"];
            weight = node["weight"];
            abilities = GetValueDatas(node["abilities"], "ability",
                "is_hidden", "slot");
            forms = GetNameUrlList(node["forms"]);
            heldItems = GetValueDatas(node["held_items"], "item",
                "version_details");
            locationAreaEncounters = node["location_area_encounters"];
            moves = GetValueDatas(node["moves"], "move",
                "version_group_details");
            Debug.Log(node["species"]);
            species = GetNameUrl(node["species"]);
            sprites = GetSprites(node["sprites"]);
            stats = GetValueDatas(node["stats"], "stat",
                "base_stat", "effort");
            types = GetValueDatas(node["types"], "type",
                "slot");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokemon/" + id;
        }

        /*/
        public override string ToString() => ToString("");
        public string ToString(string format) {
            // types
            string typesStr = "(";
            for(int i = 0; i < typesDat.Count; i++) {
                typesStr += typesDat[i].ToString("short");
            }
            typesStr += ")";
            // abilities
            string abilityStr = "(";
            for(int i = 0; i < abilitiesDat.Count; i++) {
                abilityStr += abilitiesDat[i].ToString("short");
            }
            abilityStr += ")";
            // total
            return $"[{name} =>\n order: {order},\n height: {height},\n weight: {weight},\n {statsDat},\n {speciesDat}" 
                + (format.ToLower().Equals("short") ? "" : $",\n {typesStr},\n {abilityStr}")
                + "]";
        }

        // --- Other classes --- //

        public struct Stats {
            public int hp;
            public int attack;
            public int defence;
            public int attackSpe;
            public int defenceSpe;
            public int speed;

            public Stats(int hp, int attack, int defence, int attackSpe, int defenceSpe, int speed) {
                this.hp = hp;
                this.attack = attack;
                this.defence = defence;
                this.attackSpe = attackSpe;
                this.defenceSpe = defenceSpe;
                this.speed = speed;
            }

            public override string ToString() {
                return $"(hp: {hp}, attack: {attack}, defence: {defence}, attackSpe: {attackSpe}, defenceSpe: {defenceSpe}, speed: {speed})";
            }
        }//*/
    }
}