using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class GenderData : APIResource {
        public List<ValueData> pokemonSpeciesDetails;
        public List<NameUrl> requiredForEvolution;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            pokemonSpeciesDetails = GetValueDatas(node["pokemon_species_details"],
                "pokemon_species", "rate");
            requiredForEvolution = GetNameUrlList(node["required_for_evolution"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "gender/" + id;
        }
    }
}