using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NeutralCode.PokeAPI.SimpleJSON;

namespace NeutralCode.PokeAPI {
    public class PokemonShapeData : APIResource {
        public List<TextEntry> awesomeNames;
        public List<TextEntry> names;
        public List<NameUrl> pokemonSpecies;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            awesomeNames = GetTextEntries(node["awesome_names"], "awesome_name");
            names = GetNames(node["names"]);
            pokemonSpecies = GetNameUrlList(node["pokemon_species"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokemon-shape/" + id;
        }
    }
}