using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class GrowthRateData : APIResource {
        public string formula;
        public List<TextEntry> descriptions;
        public List<LevelXp> levels;
        public List<NameUrl> species;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            JSONNode subNode, entry;
            formula = node["formula"];
            descriptions = GetTextEntries(node["descriptions"], "description");

            subNode = node["levels"];
            levels = new List<LevelXp>();
            for(int i = 0; i < subNode.Count; i++) {
                entry = subNode[i];
                levels.Add(new LevelXp(entry["level"], entry["experience"]));
            }

            species = GetNameUrlList(node["species"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "growth-rate/" + id;
        }

        public struct LevelXp {
            public int level;
            public int experience;

            public LevelXp(int level, int experience) {
                this.level = level;
                this.experience = experience;
            }
        }
    }
}