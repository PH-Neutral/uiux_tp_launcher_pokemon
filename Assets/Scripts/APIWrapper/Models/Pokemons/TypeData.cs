using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class TypeData : APIResource {
        /*/
        public Dictionary<string, float> damageToDicURL;
        public Dictionary<string, float> damageFromDicURL;
        public Dictionary<TypeData, float> damageToDic;
        public Dictionary<TypeData, float> damageFromDic;//*/

        public List<NameUrl> noDamageTo;
        public List<NameUrl> halfDamageTo;
        public List<NameUrl> doubleDamageTo;
        public List<NameUrl> noDamageFrom;
        public List<NameUrl> halfDamageFrom;
        public List<NameUrl> doubleDamageFrom;
        public List<ValueData> gameIndices;
        public NameUrl generation;
        public NameUrl moveDamageClass;
        public List<TextEntry> names;
        public List<ValueData> pokemons;
        public List<NameUrl> moves;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            JSONNode subNode;
            subNode = node["damage_relations"];
            noDamageTo = GetNameUrlList(subNode["no_damage_to"]);
            halfDamageTo = GetNameUrlList(subNode["half_damage_to"]);
            doubleDamageTo = GetNameUrlList(subNode["double_damage_to"]);
            noDamageFrom = GetNameUrlList(subNode["no_damage_from"]);
            halfDamageFrom = GetNameUrlList(subNode["half_damage_from"]);
            doubleDamageFrom = GetNameUrlList(subNode["double_damage_from"]);
            gameIndices = GetValueDatas(node["game_indices"], "generation",
                "game_index");
            generation = GetNameUrl(node["generation"]);
            moveDamageClass = GetNameUrl(node["move_damage_class"]);
            names = GetNames(node["names"]);
            pokemons = GetValueDatas(node["pokemon"], "pokemon",
                "slot");
            moves = GetNameUrlList(node["moves"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "type/" + id;
        }

        /*/
        public override string ToString() => ToString("");
        public string ToString(string format) {
            string typesToStr, typesFromStr;
            // types to
            typesToStr = "( Damage To => ";
            var typesTo = new List<TypeData>(damageToDic.Keys);
            for(int i = 0; i < typesTo.Count; i++) {
                typesToStr += $"{typesTo[i].name}: {damageToDic[typesTo[i]]}" + (i < typesTo.Count - 1 ? ", " : " ");
            }
            typesToStr += " )";
            // types from
            typesFromStr = "( Damage From => ";
            var typesFrom = new List<TypeData>(damageToDic.Keys);
            for(int i = 0; i < typesFrom.Count; i++) {
                typesFromStr += $"{typesFrom[i].name}: {damageToDic[typesFrom[i]]}" + (i < typesFrom.Count - 1 ? ", " : " ");
            }
            typesFromStr += " )";
            // total
            return $"[{name}{(format.ToLower().Equals("short") ? "" : (" => " + typesToStr + " " + typesFromStr))}]";
        }//*/
    }
}