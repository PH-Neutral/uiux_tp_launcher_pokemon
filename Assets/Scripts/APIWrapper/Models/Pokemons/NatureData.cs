using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class NatureData : APIResource {
        public NameUrl decreasedStat;
        public NameUrl increasedStat;
        public NameUrl likesFlavor;
        public NameUrl hatesFlavor;
        public List<ValueData> pokeathlonStatChanges;
        public List<ValueData> moveBattleStylePreferences;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            decreasedStat = GetNameUrl(node["decreased_stat"]);
            increasedStat = GetNameUrl(node["increased_stat"]);
            likesFlavor = GetNameUrl(node["likes_flavor"]);
            hatesFlavor = GetNameUrl(node["hates_flavor"]);
            pokeathlonStatChanges = GetValueDatas(node["pokeathlon_stat_changes"],
                "pokeathlon_stat", "max_change");
            moveBattleStylePreferences = GetValueDatas(node["move_battle_style_preferences"],
                "move_battle_style", "low_hp_preference", "high_hp_preference");
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "nature/" + id;
        }
    }
}