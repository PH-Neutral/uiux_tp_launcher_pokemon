using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class EggGroupData : APIResource {
        public List<TextEntry> names;
        public List<NameUrl> species;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            names = GetNames(node["names"]);
            species = GetNameUrlList(node["species"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "egg-group/" + id;
        }
    }
}