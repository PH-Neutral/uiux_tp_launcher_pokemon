using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class PokemonSpeciesData : APIResource {
        /*/
        public PokemonHabitatData habitat {
            get => _habitat;
            set {
                _habitat = value;
                if(_habitat != null && !_habitat.species.Contains(this)) {
                    _habitat.species.Add(this);
                }
            }
        }
        public int order;
        public bool isBaby;
        public bool isLegendary;
        public bool isMythical;
        public string shortDescription;
        public Color color;
        public EvolutionChainData evolutionChain;
        public List<PokemonSpeciesData> evolutions;
        PokemonHabitatData _habitat;//*/

        public int order;
        public int genderRate;
        public int captureRate;
        public int baseHappiness;
        public bool isBaby;
        public bool isLegendary;
        public bool isMythical;
        public int hatchCounter;
        public bool hasGenderDifferences;
        public bool formsSwitchable;
        public NameUrl growthRate;
        public List<ValueData> pokedexNumbers;
        public List<NameUrl> eggGroups;
        public NameUrl color;
        public NameUrl shape;
        public NameUrl evolvesFromSpecies;
        public NameUrl evolutionChain;
        public NameUrl habitat;
        public NameUrl generation;
        public List<TextEntry> names;
        public List<TextEntry> flavorTextEntries;
        public List<TextEntry> formDescriptions;
        public List<TextEntry> genera;
        public List<ValueData> varieties;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            order = node["order"];
            genderRate = node["gender_rate"];
            captureRate = node["capture_rate"];
            baseHappiness = node["base_happiness"];
            isBaby = node["is_baby"];
            isLegendary = node["is_legendary"];
            isMythical = node["is_mythical"];
            hatchCounter = node["hatch_counter"];
            hasGenderDifferences = node["has_gender_differences"];
            formsSwitchable = node["forms_switchable"];
            growthRate = GetNameUrl(node["growth_rate"]);
            pokedexNumbers = GetValueDatas(node["pokedex_numbers"], "pokedex",
                "entry_number");
            eggGroups = GetNameUrlList(node["egg_groups"]);
            color = GetNameUrl(node["color"]);
            shape = GetNameUrl(node["shape"]);
            evolvesFromSpecies = GetNameUrl(node["evolves_from_species"]);
            evolutionChain = new NameUrl("evolution_chain", node["evolution _chain"]["url"]);
            habitat = new NameUrl("habitat", node["habitat"]);
            generation = GetNameUrl(node["generation"]);
            names = GetNames(node["names"]);
            flavorTextEntries = GetTextEntries(node["flavor_text_entries"], "flavor_text");
            formDescriptions = GetTextEntries(node["form_descriptions"], "description");
            genera = GetTextEntries(node["genera"], "genus");
            varieties = GetValueDatas(node["varieties"], "pokemon",
                "is_default");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokemon-species/" + id;
        }

        /*/
        public override string ToString() {
            return $"({name} => habitat: {_habitat}, description: \"{shortDescription}\")";
        }//*/
    }
}