using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class PokemonFormData : APIResource {
        public int order;
        public int formOrder;
        public bool isDefault;
        public bool isBattleOnly;
        public bool isMega;
        public string formName;
        public NameUrl pokemon;
        public List<NameUrl> sprites;
        public List<ValueData> types;
        public NameUrl versionGroup;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            order = node["order"];
            formOrder = node["form_order"];
            isDefault = node["is_default"];
            isBattleOnly = node["is_battle_only"];
            isMega = node["is_mega"];
            formName = node["form_name"];
            pokemon = GetNameUrl(node["pokemon"]);
            sprites = GetSprites(node["sprites"]);
            types = GetValueDatas(node["types"], "type",
                "slot");
            versionGroup = GetNameUrl(node["version_group"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokemon-form/" + id;
        }
    }
}