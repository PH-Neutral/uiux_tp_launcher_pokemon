using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class StatData : APIResource {
        public int gameIndex;
        public bool isBattleOnly;
        public List<ValueData> affectingMovesIncrease;
        public List<ValueData> affectingMovesDecrease;
        public List<NameUrl> affectingNaturesIncrease;
        public List<NameUrl> affectingNaturesDecrease;
        public List<string> characteristics;
        public NameUrl moveDamageClass;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            JSONNode subNode;
            gameIndex = node["game_index"];
            isBattleOnly = node["is_battle_only"];

            subNode = node["affecting_moves"];
            affectingMovesIncrease = GetValueDatas(subNode["increase"], "move",
                "change");
            affectingMovesDecrease = GetValueDatas(subNode["decrease"], "move",
                "change");

            subNode = node["affecting_natures"];
            affectingNaturesIncrease = GetNameUrlList(subNode["increase"]);
            affectingNaturesDecrease = GetNameUrlList(subNode["decrease"]);

            subNode = node["characteristics"];
            characteristics = new List<string>();
            for(int i = 0; i < subNode.Count; i++) {
                characteristics.Add(subNode[i]["url"]);
            }

            moveDamageClass = GetNameUrl(node["move_damage_class"]);
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "stat/" + id;
        }
    }
}