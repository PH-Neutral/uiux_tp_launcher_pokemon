using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class AbilityData : APIResource {
        //public string shortDescription;

        public bool isMainSeries;
        public NameUrl generation;
        public List<TextEntry> names;
        public List<TextEntry> effectEntries;
        public List<TextEntry> flavorTextEntries;
        public List<ValueData> pokemons;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            isMainSeries = node["is_main_series"];
            generation = GetNameUrl(node["generation"]);
            names = GetNames(node["names"]);
            effectEntries = GetTextEntries(node["effect_entries"], "effect", "short_effect");
            flavorTextEntries = GetTextEntries(node["flavor_text_entries"], "flavor_text");
            pokemons = GetValueDatas(node["pokemon"], "pokemon", "is_hidden", "slot");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "ability/" + id;
        }
        /*/
        const int shortDescMaxLengthDisplayed = 10;
        public override string ToString() => ToString("");
        public string ToString(string format) {
            string shortDescStr = shortDescription.Length > shortDescMaxLengthDisplayed + 3 ? shortDescription.Substring(0, shortDescMaxLengthDisplayed) + "..." : shortDescription;
            return $"[{name}: \"{(format.ToLower().Equals("short") ? shortDescStr : shortDescription)}\"]";
        }//*/
    }
}