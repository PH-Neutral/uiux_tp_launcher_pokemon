using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class CharacteristicData : APIResource {
        public int geneModulo;
        public List<int> possibleValues;
        public NameUrl highestStat;
        public List<TextEntry> descriptions;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            JSONNode subNode;

            geneModulo = node["gene_modulo"];

            subNode = node["possible_values"];
            possibleValues = new List<int>();
            for(int i = 0; i < subNode.Count; i++) {
                possibleValues.Add(subNode[i]);
            }

            highestStat = GetNameUrl(node["highest_stat"]);
            descriptions = GetTextEntries(node["descriptions"], "description");
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "characteristic/" + id;
        }
    }
}