using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;

namespace NeutralCode.PokeAPI {
    public class PokeathlonStatData : APIResource {
        public List<ValueData> affectingNaturesIncrease;
        public List<ValueData> affectingNaturesDecrease;
        public List<TextEntry> names;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            JSONNode subNode;
            subNode = node["affecting_natures"];
            affectingNaturesIncrease = GetValueDatas(subNode["increase"], "nature", 
                "max_change");
            affectingNaturesIncrease = GetValueDatas(subNode["decrease"], "nature",
                "max_change");
            names = GetNames(node["names"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokeathlon-stat/" + id;
        }
    }
}