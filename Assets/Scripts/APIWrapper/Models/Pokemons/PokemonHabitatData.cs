using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NeutralCode.PokeAPI.SimpleJSON;

namespace NeutralCode.PokeAPI {
    public class PokemonHabitatData : APIResource {
        /*/
        public List<PokemonSpeciesData> species;
        List<string> _speciesURLs;//*/

        public List<TextEntry> names;
        public List<NameUrl> pokemonSpecies;

        public override void Initialize(string url, JSONNode node) {
            base.Initialize(url, node);
            names = GetNames(node["names"]);
            pokemonSpecies = GetNameUrlList(node["pokemon_species"]);
        }

        public override string GetUrl(int id) {
            return base.GetUrl(id) + "pokemon-habitat/" + id;
        }

        public override string ToString() {
            return $"({name})";
        }
    }
}