using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NeutralCode.PokeAPI.Example {
    public class ExamplePokeAPI : MonoBehaviour {
        public PokeAPIClient client;
        public void GetAll() {
            StartCoroutine(client.Request<BerryData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<BerryFirmnessData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<BerryFlavorData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<ContestTypeData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<ContestEffectData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<SuperContestEffectData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<EncounterMethodData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<EncounterConditionData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<EncounterConditionValueData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<EvolutionChainData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<EvolutionTriggerData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<GenerationData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokedexData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<VersionData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<VersionGroupData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<ItemData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<ItemAttributeData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<ItemCategoryData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<ItemFlingEffectData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<ItemPocketData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<LocationData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<LocationAreaData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PalParkAreaData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<RegionData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MachineData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MoveData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MoveAilmentData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MoveBattleStyleData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MoveCategoryData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MoveDamageClassData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MoveLearnMethodData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<MoveTargetData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<AbilityData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<CharacteristicData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<EggGroupData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<GenderData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<GrowthRateData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<NatureData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokeathlonStatData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokemonData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokemonLocationAreaData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokemonColorData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokemonFormData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokemonHabitatData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokemonShapeData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<PokemonSpeciesData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<StatData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<TypeData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
            StartCoroutine(client.Request<LanguageData>(1, (resource, resultInfo) => { Debug.Log(resultInfo + " => " + resource); }));
        }

        public void GetPokemon(int id) {
            System.DateTime start = System.DateTime.Now;
            StartCoroutine(client.Request<PokemonData>(id, (pokemon, resultInfo) => {
                Debug.Log(resultInfo + " => " + pokemon);
                Debug.Log($"nbCalls = {client.webRequestCount}; objects count = {client.GetCachedResourcesCount()}");
                double elapsed = (System.DateTime.Now - start).TotalMilliseconds * 0.001d;
                Debug.Log($">>> Elapsed time = {elapsed.ToString("0.000")}s");
            }));
        }
        public void GetPokedex(int id) {
            System.DateTime start = System.DateTime.Now;
            StartCoroutine(client.Request<PokedexData>(id, (pokedex, resultInfo) => {
                Debug.Log(resultInfo);
                Debug.Log(pokedex);
                Debug.Log($"nbCalls = {client.webRequestCount}; objects count = {client.GetCachedResourcesCount()}");
                double elapsed = (System.DateTime.Now - start).TotalMilliseconds * 0.001d;
                Debug.Log($">>> Elapsed time = {elapsed.ToString("0.000")}s");
            }));
        }
        public void GetType(int id) {
            System.DateTime start = System.DateTime.Now;
            StartCoroutine(client.Request<TypeData>(id, (resource, resultInfo) => {
                Debug.Log(resultInfo);
                Debug.Log(resource);
                Debug.Log($"nbCalls = {client.webRequestCount}; objects count = {client.GetCachedResourcesCount()}");
                double elapsed = (System.DateTime.Now - start).TotalMilliseconds * 0.001d;
                Debug.Log($">>> Elapsed time = {elapsed.ToString("0.000")}s");
            }));
        }
    }
}