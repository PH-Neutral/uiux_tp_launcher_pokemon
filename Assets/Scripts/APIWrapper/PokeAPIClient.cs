using System.Collections;
using System.Collections.Generic;
using NeutralCode.PokeAPI.SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;

namespace NeutralCode.PokeAPI {
    [System.Serializable]
    public class PokeAPIClient {
        public const string defaultWebUrl = "https://pokeapi.co/api/v2/";

        public int webRequestCount {
            get => _nbCalls;
        }

        public DebugMode debugMode = DebugMode.Basic;

        Dictionary<System.Type, Dictionary<string, APIResource>> _dataTypeDic = new Dictionary<System.Type, Dictionary<string, APIResource>>();
        Dictionary<string, Texture2D> _textureDic = new Dictionary<string, Texture2D>();
        List<IEnumerator> _requestList = new List<IEnumerator>();
        int _nbCalls = 0;

        public PokeAPIClient() {
            _nbCalls = 0;
            _dataTypeDic = new Dictionary<System.Type, Dictionary<string, APIResource>>();
            _textureDic = new Dictionary<string, Texture2D>();
            _requestList = new List<IEnumerator>();
        }

        #region PUBLIC_REQUESTS

        public IEnumerator RequestTexture(string url, System.Action<Texture2D, RequestInfo> callback) {
            var request = GetTexture(url, callback);
            _requestList.Add(request);
            if(_requestList.Count > 1) {
                yield return new WaitUntil(() => _requestList.Count == 1);
            }
            yield return request;
            _requestList.Remove(request);
        }
        public IEnumerator RequestAll<T>(System.Action startCallback = null,
            System.Action<T[], RequestInfo> arrayCallback = null, 
            System.Action<T, RequestInfo> individualCallback = null) where T : APIResource, new() 
            => RequestMany(0, startCallback, individualCallback, arrayCallback);
        public IEnumerator RequestMany<T>(int maxCount, System.Action startCallback = null,
            System.Action<T, RequestInfo> individualCallback = null,
            System.Action<T[], RequestInfo> arrayCallback = null) where T : APIResource, new() {

            startCallback?.Invoke();
            bool requestAll = maxCount <= 0;
            string url = new ResultSet<T>().GetUrl();
            RequestInfo userInfo = RequestInfo.UnknownStatus;
            List<T> dataSet = new List<T>();
            int dataCount = -1;
            do {
                ResultSet<T> set = null;
                yield return GetData<ResultSet<T>>(url, (data, info) => {
                    userInfo = info;
                    set = data;
                });
                if(userInfo != RequestInfo.Success) break;
                dataCount = set.totalCount;
                foreach(var item in set.results) {
                    if(!requestAll && dataSet.Count >= maxCount) break;
                    yield return GetData<T>(item.url, (data, info) => {
                        if(data != null) {
                            dataSet.Add(data);
                        }
                        individualCallback?.Invoke(data, info);
                    });
                }
                url = set.nextSet;
            } while(!string.IsNullOrEmpty(url) && (requestAll || dataSet.Count < maxCount));
            if(userInfo == RequestInfo.Success) {
                if(requestAll ? dataSet.Count != dataCount : dataSet.Count != maxCount) {
                    userInfo = RequestInfo.PartialError;
                }
            }
            arrayCallback?.Invoke(dataSet.ToArray(), userInfo);
            yield break;
        }
        public IEnumerator RequestSet<T>(System.Action<ResultSet<T>, RequestInfo> callback = null) where T : APIResource, new() => Request<ResultSet<T>>(new ResultSet<T>().GetUrl(), callback);
        public IEnumerator Request<T>(int id, System.Action<T, RequestInfo> callback = null) where T : APIResource, new() => Request(new T().GetUrl(id), callback);
        public IEnumerator Request<T>(string url, System.Action<T, RequestInfo> callback = null) where T : APIResource, new() {
            yield return GetData(url, callback);
            
            /*var request = GetData(url, callback);
            _requestList.Add(request);
            if(_requestList.Count > 1) {
                yield return new WaitUntil(() => _requestList.Count == 1);
            }
            yield return request;
            _requestList.Remove(request);//*/
        }

        #endregion

        #region GENERIC_METHODS

        IEnumerator GetData<T>(string url, System.Action<T, RequestInfo> callback) where T : APIResource, new() {
            if(string.IsNullOrEmpty(url)) {
                HandleCallback(callback, url, null, APIResult.IncorrectURL, RequestInfo.IncorrectURL);
                yield break;
            }
            T data = GetCachedData<T>(url);
            if(data != null) {
                HandleCallback(callback, url, data, APIResult.AlreadyCached, RequestInfo.Success);
                yield break;
            }
            UnityWebRequest request = UnityWebRequest.Get(url);
            yield return request.SendWebRequest();
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) {
                Debug.Log($"> REQUESTED a {typeof(T).Name} !{(debugMode == DebugMode.Detailed ? $" (url: {url})" : "")}");
            }
            _nbCalls++;
            if(request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError) {
                HandleCallback(callback, url, null, APIResult.WebError, RequestInfo.WebError);
                yield break;
            }
            var node = JSON.Parse(request.downloadHandler.text);
            T resource = new T();
            resource.Initialize(url, node);
            CacheData(resource);
            HandleCallback(callback, url, resource, APIResult.WebSuccess, RequestInfo.Success);
            yield break;
        }
        void CacheData<T>(T data) where T : APIResource {
            if(data == null) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to cache null data => type: {typeof(T).Name}");
                return;
}
            if(string.IsNullOrEmpty(data.url)) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to cache data with an incorrect url => type: {typeof(T).Name}");
                return;
            }
            if(!_dataTypeDic.ContainsKey(typeof(T))) AddDataList<T>();
            if(!_dataTypeDic[typeof(T)].ContainsKey(data.url)) {
                _dataTypeDic[typeof(T)].Add(data.url, data);
            }
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) {
                Debug.Log($"> CACHED a {typeof(T).Name} !{(debugMode == DebugMode.Detailed ? $" (url: {data.url})" : "")}");
            }
        }
        public T GetCacheData<T>(int id) where T : APIResource, new() => GetCachedData<T>(new T().GetUrl(id));
        public T GetCachedData<T>(string url) where T : APIResource, new() {
            if(string.IsNullOrEmpty(url)) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to fetch cached data using an incorrect url => type: {typeof(T).Name}");
                return null;
            }
            if(!_dataTypeDic.ContainsKey(typeof(T))) {
                AddDataList<T>();
                return null;
            }
            var dataDic = _dataTypeDic[typeof(T)];
            if(!dataDic.ContainsKey(url)) return null;
            return dataDic[url] as T;
        }
        void HandleCallback<T>(System.Action<T, RequestInfo> callback, string url, T resource, APIResult info, RequestInfo userInfo) where T : APIResource {
            if(info == APIResult.WebError || info == APIResult.IncorrectURL) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"{typeof(T).Name} => {info} at url = \"{url}\"");
            } else {
                if(debugMode == DebugMode.Detailed) Debug.Log($"{typeof(T).Name} => {info}");
            }
            callback?.Invoke(resource, userInfo);
        }
        public IEnumerator GetTexture(string url, System.Action<Texture2D, RequestInfo> callback) {
            Texture2D tex;
            if(string.IsNullOrEmpty(url)) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to request data using an incorrect url => type: Texture2D");
                HandleCallback(callback, null, null, APIResult.IncorrectURL, RequestInfo.IncorrectURL);
                yield break;
            }
            if((tex = GetCachedTexture(url)) != null) {
                HandleCallback(callback, url, tex, APIResult.AlreadyCached, RequestInfo.Success);
                yield break;
            }
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
            yield return request.SendWebRequest();
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) {
                Debug.Log($"> REQUESTED a Texture2D !{(debugMode == DebugMode.Detailed ? $" (url: {url})" : "")}");
            }
            _nbCalls++;
            if(request.result != UnityWebRequest.Result.Success) {
                HandleCallback(callback, url, null, APIResult.WebError, RequestInfo.WebError);
                yield break;
            }
            CacheTexture(tex, url);
            HandleCallback(callback, url, ((DownloadHandlerTexture)request.downloadHandler).texture, APIResult.WebSuccess, RequestInfo.Success);
            yield break;
        }
        void CacheTexture(Texture2D tex, string url) {
            if(tex == null) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to cache null data => type: Texture2D");
                return;
            }
            if(string.IsNullOrEmpty(url)) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to cache data using an incorrect url => type: Texture2D");
                return;
            }
            if(!_textureDic.ContainsKey(url)) {
                _textureDic.Add(url, tex);
            }
            if(debugMode != DebugMode.NoLog && debugMode != DebugMode.WarningOnly) {
                Debug.Log($"> CACHED a Texture2D !{(debugMode == DebugMode.Detailed ? $" (url: {url})" : "")}");
            }
        }
        public Texture2D GetCachedTexture(string url) {
            if(string.IsNullOrEmpty(url)) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Trying to fetch cached data using an incorrect url => type: Texture2D");
                return null;
            }
            if(!_textureDic.ContainsKey(url)) return null;
            return _textureDic[url];
        }
        void HandleCallback(System.Action<Texture2D, RequestInfo> callback, string url, Texture2D resource, APIResult info, RequestInfo userInfo) {
            if(info == APIResult.WebError || info == APIResult.IncorrectURL) {
                if(debugMode != DebugMode.NoLog) Debug.LogWarning($"Texture2D => {info} at url = \"{url}\"");
            } else {
                if(debugMode == DebugMode.Detailed) Debug.Log($"Texture2D => {info}");
            }
            callback?.Invoke(resource, userInfo);
        }
        void AddDataList<T>() where T : APIResource {
            if(_dataTypeDic.ContainsKey(typeof(T))) return;
            _dataTypeDic.Add(typeof(T), new Dictionary<string, APIResource>());
        }
        public int GetCachedResourcesCount() {
            int result = _textureDic.Count;
            foreach(var dic in _dataTypeDic.Values) {
                result += dic.Count;
            }
            return result;
        }
        public T[] GetArray<T>() where T : APIResource {
            if(!_dataTypeDic.ContainsKey(typeof(T))) return null;
            return new List<APIResource>(_dataTypeDic[typeof(T)].Values).ConvertAll(x => (T)x).ToArray();
        }

        #endregion
    }
    public enum RequestInfo {
        UnknownStatus,
        Success,
        UndefinedError,
        PartialError,
        IncorrectURL,
        WebError
    }
    public enum APIResult {
        None,
        ResourceTypeNotFound,
        AlreadyCached,
        IncorrectURL,
        WebError,
        WebSuccess
    }
    public enum DebugMode {
        NoLog,
        WarningOnly,
        Basic,
        Detailed
    }
}